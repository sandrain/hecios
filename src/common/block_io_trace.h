#ifndef BLOCK_IO_TRACE_H
#define BLOCK_IO_TRACE_H

/** cachesim extension */

#include <cstddef>
#include <iostream>
#include <string>
#include <omnetpp.h>

class BlockIOTrace
{
    public:

    protected:

    private:
};

#endif  /** BLOCK_IO_TRACE_H */

/*
 * Local variables:
 *  indent-tabs-mode: nil
 *  c-indent-level: 4
 *  c-basic-offset: 4
 * End:
 *
 * vim: ts=8 sts=4 sw=4 expandtab
 */
